Travel API mock 
===============

[![Build Status](https://drone.io/bitbucket.org/afklmdevnet/simple-travel-api-mock/status.png)](https://drone.io/bitbucket.org/afklmdevnet/simple-travel-api-mock/latest)

Clone this repo and start the mock (on windows systems use the gradlew.bat file):

`./gradlew bootRun`

to list all tasks:

`./gradlew tasks`

The mock resources are protected and require authentication through OAuth 2 to gain access. The following credentials can be used to connect to the service:
 
- client-id: travel-api-client
- secret: psw
 
The OAuth2 grant type required to retrieve a token is **'client_credentials'**.
 
The OAuth2 token endpoint after startup is:
 
`http://localhost:8080/oauth/token`
 
Resource endpoints:
-------------------

**Retrieve a list of airports**:

`http://localhost:8080/airports`

Query params:

- size: the size of the result
- page: the page to be selected in the paged response
- lang: the language, supported ones are nl and en
- term: A search term that searches through code, name and description.

**Retrieve a specific airport**:

`http://localhost:8080/airports/{code}`

Query params:

- lang: the language, supported ones are **nl** and **en**

**Retrieve a fare offer**:

`http://localhost:8080/fares/{origin_code}/{destination_code}`

Query params:

- currency: the requested resulting currency, supported ones are EUR and USD
 

Changes:
-------
These are the changes i've done to the project given the time.

**frontend**
 - created the frontend location: simple-travel-ui
 - When served the searchbar component searches for matching airports via the simplet-travel-api
 - OAuth is done on application level and the token checks if it is still valid. If not it requests a new token before it continues
 - Every config is stored in environment/environment
 
**backend**
 - Added a cors policy so that the frontend is able to send it's request. (This should change to only development environment instead of all)
 - Added a security policy which exposes /prometheus for dashboarding. 
 - Added Requesttiminginterceptor and requestcounterinterceptor for the prometheus dashboard
 - Added webmvcConfig to add the interceptors
 
 **dashboard**
 
 Instead of building my own dashboard I've chosen to use some opensource packages which makes it possible to create monitoring stats from scraping web-endpoint
 I created a very simple docker compose file which starts both dashboards
 
 Run: `docker-compose up`
 to start prometheus and grafana.
 
 The dashboards still need to be configured and created because it will run on another system. But it gives a general idea where i wanted to go with this ;)
 
 **Creating Module**
 
 I've splitted the two projects and created 2 seperate gradle files. 
 The API relies on the build of the frontend which builds a jar. 
 I was unable given the time (and experience) to get the jar exposed with the build. 