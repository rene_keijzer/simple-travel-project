package main.java.com.afkl.exercises.spring;

import main.java.com.afkl.exercises.spring.common.RequestCounterInterceptor;
import main.java.com.afkl.exercises.spring.common.RequestTimingInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


import java.util.Arrays;
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {


    //@Todo Bad for testing purposes, but will do for now
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new RequestCounterInterceptor());
        registry.addInterceptor(new RequestTimingInterceptor());
    }

}
