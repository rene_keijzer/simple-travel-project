import { Component, OnInit } from '@angular/core';
import { Airport } from 'src/models/Airport';
import {Fare} from 'src/models/Fare';
import { FareService } from 'src/app/services/fare.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  state = 1;
  selectedFare: Fare;
  fromAirport:  Airport;
  destinationAirport: Airport;
  constructor(private fareService:FareService) { }

  ngOnInit() {
  }

  changeFrom(airport:Airport){
    this.fromAirport = airport;
  }

  changeDestination(airport: Airport){
    
    this.destinationAirport = airport;
  }
  submit(){
    this.state++;
    this.findFare(this.fromAirport, this.destinationAirport);
  }

  findFare(origin:Airport, destination:Airport){
    this.fareService.getFare(origin, destination).subscribe((fare: Fare)=>{
      this.selectedFare = fare;
    });
  }

  back(){
    this.state--;
    this.fromAirport=undefined;
    this.destinationAirport=undefined;
    this.selectedFare=undefined;
  }
}
