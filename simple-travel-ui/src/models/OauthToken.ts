import { Adapter } from 'src/helpers/adapter';
import {Injectable} from '@angular/core';


@Injectable({
    providedIn:'root'
})
export class OauthToken{
    accessToken: string;
    tokenType: string;
    expiresIn: number;
    obtained:number;
    scope:string;

    constructor(accessToken: string, tokenType: string, expiresIn: number, obtained:number, scope:string){
        this.accessToken = accessToken;
        this.tokenType = tokenType;
        this.expiresIn = expiresIn * 1000; 
        this.obtained = obtained;
        this.tokenType = tokenType;
    }

    isValid(): boolean{        
        return Date.now() < (this.obtained + this.expiresIn);
    }

    getAuthHeader(){
        return `${this.tokenType} ${this.accessToken}`;
    }
}


export class OauthTokenAdapter implements Adapter<OauthToken>{
    adapt(item: any): OauthToken{
        return new OauthToken(item.access_token, item.token_type, item.expires_in, Date.now(), item.scope);
    }
}